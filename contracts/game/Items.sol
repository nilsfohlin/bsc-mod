// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "interfaces/IChromiaNetResolver.sol";

contract Items is ERC721, AccessControl {

    bytes32 public constant RESOURCES_HANDLER = keccak256("RESOURCES_HANDLER");
    IChromiaNetResolver private CNR;

    mapping(uint => uint) public typeCount;

    constructor (IChromiaNetResolver _CNR) ERC721 ("Mines of Dalarnia Items", "MoD-Item") {
        CNR = _CNR;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    event Craft (address crafter, uint[] burn, uint[] mint);

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721, AccessControl) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    modifier isResourcesHandler(){
        require(hasRole(RESOURCES_HANDLER, msg.sender),"Tx not from resourcesHandler");
        _;
    }

    function createItemType(uint _type) external isResourcesHandler() returns (bool){
        require(_type < 10000000000 && _type >= 1000001000, "Invalid prefix");
        require(typeCount[_type] == 0, "Prefix taken");
        typeCount[_type] = 1;
        return true;
    }

    function dev_mint(uint256 _type) public { ///////////////////////////////////////////////////////////////////////////////////////////REMOVE 
        require(hasRole(DEFAULT_ADMIN_ROLE, msg.sender),"Tx not from admin");
        _mint(msg.sender, fullPrefix(_type) + typeCount[_type]);
        typeCount[_type]++;
    }

    function mint(address _to, uint256[] memory _mintTypes) external isResourcesHandler() returns (bool){
        for (uint256 index = 0; index < _mintTypes.length; index++) {
            uint token = fullPrefix(_mintTypes[index]) + typeCount[_mintTypes[index]];
            _safeMint(_to, token);
            typeCount[_mintTypes[index]]++;
        }
        return true;
    }

    function burn(address _from, uint256[] memory _burnItemIds) external isResourcesHandler() returns (bool){
        _burnMultiple(_from, _burnItemIds);
        return true;
    }

    function burnAndMint(address _fromTo, uint256[] memory _mintTypes, uint256[] memory _burnItemIds) external isResourcesHandler() returns (bool){
        _burnMultiple(_fromTo, _burnItemIds);
        
        uint[] memory emitArr = new uint[](_mintTypes.length);
        for (uint256 index = 0; index < _mintTypes.length; index++) {
            uint token = fullPrefix(_mintTypes[index]) + typeCount[_mintTypes[index]];
            _safeMint(_fromTo, token);
            typeCount[_mintTypes[index]]++;
            emitArr[index] = token;
        }
        emit Craft(_fromTo, _burnItemIds, emitArr);
        return true;
    }

    function _burnMultiple(address _from, uint[] memory _burnItemIds) internal {
        for (uint256 index = 0; index < _burnItemIds.length; index++) {
            require(_isApprovedOrOwner(_msgSender(), _burnItemIds[index]), "ERC721: caller is not owner nor approved");
            require(ownerOf(_burnItemIds[index]) == _from, "Not owner of item input");
            _burn(_burnItemIds[index]);
        }
    }

    function fullPrefix(uint _type)internal pure returns (uint){
        return _type * (10 ** 68);
    }

    function prefixExists(uint _prefix) external view returns (bool){
        if(typeCount[_prefix] > 0){
            return true;
        }else{
            return false;
        }
    }

    function tokenURI(uint _tokenId) public view override returns (string memory) {
        require(_exists(_tokenId), "ERC721Metadata: URI query for nonexistent token");
        return IChromiaNetResolver(CNR).getNFTURI(address(this), _tokenId);
    }
}
