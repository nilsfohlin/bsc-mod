// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "interfaces/IPlanetPlot.sol";
import "interfaces/IResources.sol";
import "interfaces/IItems.sol";
import "interfaces/IPlanetPlotHandler.sol";

contract ResourcesHandler is AccessControl, ReentrancyGuard, Pausable {

    bytes32 public constant RECIPE_CREATOR = keccak256("RECIPE_CREATOR");
    bytes32 public constant GAME_CONTROL = keccak256("GAME_CONTROL");
    bytes32 public constant RESOURCE_ITEM_CREATOR = keccak256("RESOURCE_ITEM_CREATOR");
    bytes32 public constant GAME_PAUSER = keccak256("GAME_PAUSER");

    address private MoDTaxAccount;
    address public planetPlotHandler;
    IPlanetPlot public planetPlot;
    IResources public resources; 
    IItems public items;
    IERC20 public DAR;

    mapping(uint=>Crafting_recipe) public craftingRecipes;
    mapping(uint=>Plot_ru_recipe) public plotRURecipes;
    mapping(uint=>Planet_pass_recipe) public planetPassRecipes;
    
    // Used for combining / minting new in game artifacts. 
    // Needs to have one input and one output.
    // Should only be able to do valid recipies - ingredients exist.
    // Once a recipie is there anyone can "use it"
    // Deletable
    struct Crafting_recipe {
        uint[] resouceInputIds; // erc1155
        uint[] resouceInputAmounts;

        uint[] itemInputPrefixes; // erc721

        uint darInputAmount; // erc20

        uint[] resouceOutputIds; // erc1155
        uint[] resouceOutputAmounts;

        uint[] itemOutputPrefixes; // erc721
    }

    // Used to upgrade/replenish a plot with digs
    // Needs to have one input and one output.
    // Can be called by anyone
    struct Plot_ru_recipe {
        uint[] resouceInputIds; // erc1155
        uint[] resouceInputAmounts;

        uint darInputAmount; // erc20

        uint max;
        uint add;
    }

    // Used to sell planet pass ports
    // Not NFTs 
    struct Planet_pass_recipe {
        uint darInputAmount; // erc20
        uint[] planetIds;
    }

    event RecipeEvent(uint recipeId, uint8 reciepeType, bool add);
    event CraftEvent(address crafter, uint recipeId);
    event ReplenishUpgradeEvent(uint plotId, uint recipeId);
    event PlanetPassEvent(address player, uint[] planetIds);

    constructor (IResources _resources, IItems _items, IERC20 _DAR, address _MoDTaxAccount, IPlanetPlot _planetPlot) {
        resources = _resources;
        items = _items;
        DAR = _DAR;
        MoDTaxAccount = _MoDTaxAccount;
        planetPlot = _planetPlot;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    modifier isRecipeCreator(){
        require(hasRole(RECIPE_CREATOR, msg.sender),"RECIPE_CREATOR required");
        _;
    }

    modifier isGameController(){
        require(hasRole(GAME_CONTROL, msg.sender),"GAME_CONTROL required");
        _;
    }

    modifier isResourceItemCreator(){
        require(hasRole(RESOURCE_ITEM_CREATOR, msg.sender),"RESOURCE_ITEM_CREATOR required");
        _;
    }

    modifier isGamePauser(){
        require(hasRole(GAME_PAUSER, msg.sender),"GAME_PAUSER required");
        _;
    }

    // Creates new resource type erc1155
    function createResource(string memory _resourceName) public isResourceItemCreator(){
        IResources(resources).createResource(_resourceName);
    }

    // Creates new item type erc721
    function createItem(uint _prefix) public isResourceItemCreator(){
        IItems(items).createItemType(_prefix);
    }

    function addCraftingRecipe(
        uint _recipeId, 
        uint[] memory _RIIds,
        uint[] memory _RIAmounts,
        uint[] memory _IIPrefixes,
        uint _DIAmount,
        uint[] memory _ROIds,
        uint[] memory _ROAmounts,
        uint[] memory _IOPrefixes
         ) public isRecipeCreator(){
        require(!recipeExists(_recipeId, 1), "Recipe id taken");

        require(_RIIds.length > 0 || _IIPrefixes.length > 0 || _DIAmount > 0, "Must have 1 input+");
        require(_ROIds.length > 0 || _IOPrefixes.length > 0,"Must have 1 output+");

        Crafting_recipe storage recipe  = craftingRecipes[_recipeId];

        recipe.resouceInputIds = checkResourceIds(_RIIds);
        recipe.resouceInputAmounts = validAmountArray(_RIAmounts, _RIIds.length);

        recipe.itemInputPrefixes = checkItemPrefixes(_IIPrefixes);

        recipe.darInputAmount = _DIAmount;

        recipe.resouceOutputIds = checkResourceIds(_ROIds);
        recipe.resouceOutputAmounts = validAmountArray(_ROAmounts, _ROIds.length);

        recipe.itemOutputPrefixes = checkItemPrefixes(_IOPrefixes);

        emit RecipeEvent(_recipeId, 1, true);
    }
        
    function removeCraftingRecipe(uint _recipeId) public isRecipeCreator(){
        require(recipeExists(_recipeId, 1));
        delete craftingRecipes[_recipeId];
        emit RecipeEvent(_recipeId, 1, false);
    }

    function addPlotRURecipe(
        uint _recipeId, 
        uint[] memory _RIIds,
        uint[] memory _RIAmounts,
        uint _DIAmount,
        uint _max,
        uint _add
         ) public isRecipeCreator(){
        require(!recipeExists(_recipeId, 2), "recipe id taken");
        require(_add > 0, "_add must be greater than zero");
        require((_RIIds.length > 0 || _DIAmount > 0), "Must have 1 input+");

        Plot_ru_recipe storage recipe = plotRURecipes[_recipeId];

        recipe.resouceInputIds = checkResourceIds(_RIIds);
        recipe.resouceInputAmounts = validAmountArray(_RIAmounts, _RIIds.length);

        recipe.darInputAmount = _DIAmount;

        recipe.max = _max;
        recipe.add = _add;
        
        emit RecipeEvent(_recipeId, 2, true);
    }

    function removePlotRURecipe(uint _recipeId) public isRecipeCreator(){
        require(recipeExists(_recipeId, 2));
        delete plotRURecipes[_recipeId];
        emit RecipeEvent(_recipeId, 2, false);
    }

    function addPlanetPassRecipe(
        uint _recipeId, 
        uint _DIAmount,
        uint[] memory _planetIds
         ) public isRecipeCreator(){
        require(!recipeExists(_recipeId, 3), "recipe id taken");
        require(_DIAmount > 0, "DAR input cant be 0");
        require (_planetIds.length > 0, "Must have 1+ planet output");

        planetPassRecipes[_recipeId].darInputAmount = _DIAmount;
        planetPassRecipes[_recipeId].planetIds = checkPlanetIds(_planetIds);
        
        emit RecipeEvent(_recipeId, 3, true);
    }

    function removePlanetPassRecipe(uint _recipeId) public isRecipeCreator(){
        require(recipeExists(_recipeId, 3));
        delete planetPassRecipes[_recipeId];
        emit RecipeEvent(_recipeId, 3, false);
    }

    function recipeExists(uint _recipeId, uint8 _type) public view returns (bool){
        if(_type == 1){
            if (craftingRecipes[_recipeId].resouceOutputIds.length == 0 && craftingRecipes[_recipeId].itemOutputPrefixes.length == 0) {
                return false;
            } else {
                return true;
            }
        }else if (_type == 2){
            if (plotRURecipes[_recipeId].add == 0) {
                return false;
            } else {
                return true;
            }
        }else{
            if (planetPassRecipes[_recipeId].planetIds.length == 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    function validAmountArray(uint[] memory _arr, uint _l) internal pure returns (uint[] memory){
        require (_arr.length == _l, "Id/pre arr not same length as amount arr");
        for (uint256 index = 0; index < _arr.length; index++) {
            require(_arr[index] > 0, "Cant be zero amount");
        }
        return _arr;
    }

    function checkResourceIds(uint[] memory _rIds) internal view returns (uint[] memory){
        if(_rIds.length > 0){
            uint count = IResources(resources).getResourceCount();
            for (uint256 index = 0; index < _rIds.length; index++) {
                require(_rIds[index] < count, "Resource not exist");
                for (uint256 j = 0; j < _rIds.length; j++) {
                    if(index != j){
                        require(_rIds[index] != _rIds[j], "Can not have the same _rId input multiple times");
                    }
                }
            }
        }
        return _rIds;
    }

    function checkItemPrefixes(uint[] memory _iPrefixes) internal view returns (uint[] memory){
        if(_iPrefixes.length > 0){   
            for (uint256 index = 0; index < _iPrefixes.length; index++) {
                require(IItems(items).prefixExists(_iPrefixes[index]), "Item not exist");
            }
        }
        return _iPrefixes;
    }

    function checkPlanetIds(uint[] memory _planetIds) internal view returns (uint[] memory){
        for (uint256 index = 0; index < _planetIds.length; index++) {
            require(IPlanetPlot(planetPlot).planetExists(_planetIds[index]), "Planet not exist");
        }
        return _planetIds;
    }

    // craft should "send/burn" all inputs and mint all outputs
    function craft(uint _recipeId, uint[] memory _tokenIds) public nonReentrant() whenNotPaused(){
        require(recipeExists(_recipeId, 1), "Recipe id not exist");

        if (craftingRecipes[_recipeId].darInputAmount > 0 ){
            require(IERC20(DAR).transferFrom(msg.sender, MoDTaxAccount, craftingRecipes[_recipeId].darInputAmount));
        } 
        if (craftingRecipes[_recipeId].resouceInputIds.length > 0){
            require(IResources(resources).burnBatch(msg.sender, craftingRecipes[_recipeId].resouceInputIds, craftingRecipes[_recipeId].resouceInputAmounts));
        }
        // Safety with items for the server to be able to replicate item stats if lost
        if (craftingRecipes[_recipeId].itemInputPrefixes.length > 0 && craftingRecipes[_recipeId].itemOutputPrefixes.length > 0){
            itemInputCheck(_recipeId, _tokenIds);
            require(IItems(items).burnAndMint(msg.sender, craftingRecipes[_recipeId].itemOutputPrefixes, _tokenIds));
        }else{
            if (craftingRecipes[_recipeId].itemInputPrefixes.length > 0){
                itemInputCheck(_recipeId, _tokenIds);
                require(IItems(items).burn(msg.sender, _tokenIds));
            }
            if (craftingRecipes[_recipeId].itemOutputPrefixes.length > 0){
                require(IItems(items).mint(msg.sender, craftingRecipes[_recipeId].itemOutputPrefixes));
            }
        }
        if (craftingRecipes[_recipeId].resouceOutputIds.length > 0){
            require(IResources(resources).mintBatch(msg.sender, craftingRecipes[_recipeId].resouceOutputIds, craftingRecipes[_recipeId].resouceOutputAmounts));
        }
        emit CraftEvent(msg.sender, _recipeId);
    }

    function itemInputCheck(uint _recipeId, uint[] memory _tokenIds) internal view {
        require(craftingRecipes[_recipeId].itemInputPrefixes.length == _tokenIds.length, "Recipe - tokenIds arr no match");
        for (uint256 i = 0; i < _tokenIds.length; i++) {
            require(craftingRecipes[_recipeId].itemInputPrefixes[i] == prefixStrip(_tokenIds[i]), "Token, recipe prefix mismatch");
        }
    }

    function prefixStrip(uint _tokenId) internal pure returns (uint) {
        return _tokenId / (10**68);
    }

    function plotRU(uint _recipeId, uint _plotTokenId) public nonReentrant() whenNotPaused(){
        require(recipeExists(_recipeId, 2), "Recipe id not exist");

        if(plotRURecipes[_recipeId].resouceInputIds.length > 0){
            require(IResources(resources).burnBatch(msg.sender, plotRURecipes[_recipeId].resouceInputIds, plotRURecipes[_recipeId].resouceInputAmounts));
        } 
        if (plotRURecipes[_recipeId].darInputAmount > 0){
            require(IERC20(DAR).transferFrom(msg.sender, MoDTaxAccount, plotRURecipes[_recipeId].darInputAmount));
        } 
        if (plotRURecipes[_recipeId].max > 0){
            require(IPlanetPlot(planetPlot).plotMax(_plotTokenId) == plotRURecipes[_recipeId].max, "Plot max, recipe max mismatch");
            require(IPlanetPlotHandler(planetPlotHandler).upgradePlotMax(_plotTokenId, (plotRURecipes[_recipeId].max + plotRURecipes[_recipeId].add)));
        } 
        else {
            require(IPlanetPlotHandler(planetPlotHandler).replenishPlot(_plotTokenId, plotRURecipes[_recipeId].add));
        }
        emit ReplenishUpgradeEvent(_recipeId, _plotTokenId);
    }

    function planetPass(uint _recipeId) public nonReentrant() whenNotPaused(){
        require(recipeExists(_recipeId, 3), "Recipe id not exist");
        require(IERC20(DAR).transferFrom(msg.sender, MoDTaxAccount, planetPassRecipes[_recipeId].darInputAmount));
        IPlanetPlotHandler(planetPlotHandler).setPlayerPlanetPass(msg.sender, planetPassRecipes[_recipeId].planetIds);

        emit PlanetPassEvent(msg.sender, planetPassRecipes[_recipeId].planetIds);
    }
    
    function updatePlanetPlotHandler (address _planetPlotHandler) public isGameController(){
        planetPlotHandler = _planetPlotHandler;
    }
    function updateTaxAccount(address _MoDTaxAccount) public isGameController(){
        MoDTaxAccount = _MoDTaxAccount;
    }

    function pauseHandler() public isGamePauser(){
        _pause();
    }
    function unpauseHandler() public isGamePauser(){
        _unpause();
    }

    function removeContractFromChain(address payable _des) public {
        require(hasRole(DEFAULT_ADMIN_ROLE, msg.sender));
        selfdestruct(_des);
    }
}