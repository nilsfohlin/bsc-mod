// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract Resources is ERC1155, AccessControl {

    bytes32 public constant RESOURCES_HANDLER = keccak256("RESOURCES_HANDLER");

    string [] public resourceCatalogue;

    mapping(string => uint) public resourcesToId;

    constructor () ERC1155 ("TESTNET_BETA") {
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC1155, AccessControl) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    modifier isResourcesHandler(){
        require(hasRole(RESOURCES_HANDLER, msg.sender),"Tx not from resourcesHandler");
        _;
    }

    function createResource (string memory _resourceName) external isResourcesHandler {
        for (uint256 index = 0; index < resourceCatalogue.length; index++) {
            require(keccak256(abi.encodePacked((_resourceName))) != (keccak256(abi.encodePacked((resourceCatalogue[index])))), "Resource name already taken");
        }

        resourceCatalogue.push(_resourceName);
        resourcesToId[_resourceName] = (resourceCatalogue.length - 1);
    }

    function mintBatch(address _to, uint[] memory _resources, uint[] memory _amounts) external isResourcesHandler() returns (bool){
        for (uint256 index = 0; index < _resources.length; index++) {
            require(_resources[index] < resourceCatalogue.length, "Trying to mint non-existant resource");
        }
        _mintBatch(_to, _resources , _amounts, "");
        return true;
    }
 // should not be able to burn without approval from owner
    function burnBatch(address _from, uint256[] memory _ids, uint256[] memory _amounts) external isResourcesHandler() returns (bool) {
        require(
            _from == _msgSender() || isApprovedForAll(_from, _msgSender()),
            "ERC1155: caller is not owner nor approved"
        );
        _burnBatch(_from, _ids, _amounts);
        return true;
    }
    
    function getResourceCount() external view returns (uint){
        return resourceCatalogue.length;
    }
}