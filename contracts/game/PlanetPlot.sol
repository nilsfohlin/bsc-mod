// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "interfaces/IChromiaNetResolver.sol";

contract PlanetPlot is ERC721, AccessControl {

    IChromiaNetResolver private CNR;
    bytes32 public constant PLANET_PLOT_HANDLER = keccak256("PLANET_PLOT_HANDLER");

    uint digsPerTimeUnit = 1;
    uint replenishTimeUnit = 1 days;

    mapping(uint => uint) public planetSide;
    mapping(uint => Plot) public plotState;
    mapping(uint => bool) public freePlanetPass;
    mapping(address => mapping(uint => bool)) public planetPass;

    struct Plot { uint max; uint left; uint rent; bool open; uint latestReplenish; }

    constructor (IChromiaNetResolver _CNR) ERC721 ("Mines of Dalarnia Planets", "MoD-Planet"){
        CNR = _CNR;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721, AccessControl) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    modifier isPlanetPlotHandler(){
        require(hasRole(PLANET_PLOT_HANDLER, msg.sender),"Tx not from planetPlotHandler");
        _;
    }

    function createPlanet (uint _sideLength, uint _id) external isPlanetPlotHandler(){
        require(_id >= 100 && _id < 1000, "planet id must be between 100 and 1000");
        require(planetSide[_id] == 0, "Planet id taken");
        require(_sideLength > 0 && _sideLength % 10 == 0 && _sideLength <= 310, "_sideLength must be greater than 0 and divisible by 100 and less than 310");
        planetSide[_id] = _sideLength;
    }

    function mintPlotRegion(address _to, uint _planetId, uint _region) external isPlanetPlotHandler(){
        uint side = planetSide[_planetId];
        require(side != 0, "PlanetId does not exist");
        uint rc = side / 10;
        require(_region <= rc*rc && _region > 0, "Invalid region");
        
        uint yRegion;
        uint xRegion;

        if(_region % rc != 0){
            xRegion = (((_region % rc) - 1) * 10) + 1;
            yRegion = ((_region / rc) * 10) + 1;
        }else{
            xRegion = side - 9;
            yRegion = ((_region / rc) * 10) - 9;
        }

        for (uint256 x = xRegion; x < xRegion + 10; x++) {
            for (uint256 y = yRegion; y < yRegion + 10; y++) {
                uint idToMint = _planetId * 1000_000_000;
                idToMint = idToMint + _region * 1000_000;
                idToMint = idToMint + x * 1000;
                idToMint = idToMint + y;
                _safeMint(_to, idToMint);
                plotState[idToMint] = Plot({max:100, left:100, rent: 1000_000, open: true, latestReplenish: block.timestamp});
            }
        }
    }

    function openRentPlot(address _renter, uint _plotId, uint8 _digsToOpen) external isPlanetPlotHandler() returns (bool){
        require(_digsToOpen != 0, "Can not open zero digs");
        _autoReplenish(_plotId);
        Plot memory state = plotState[_plotId];
        require(state.left >= _digsToOpen, "Plot does not have this amount of digs");
        uint planetId = _plotId / 1000_000_000;
        if(!freePlanetPass[planetId]){
            require(planetPass[_renter][planetId] == true, "Renter does not have a valid pass for this planet");
        }

        state.left =  state.left - _digsToOpen;
        plotState[_plotId] = state;
        return true;
    }

    function _autoReplenish(uint _plotId) internal {
        if(digsPerTimeUnit > 0){
            uint daysToReplenish = (block.timestamp - plotState[_plotId].latestReplenish) / replenishTimeUnit;

            if(daysToReplenish > 0){
                _replenishPlot(_plotId, daysToReplenish * digsPerTimeUnit);
                plotState[_plotId].latestReplenish = block.timestamp;
            }   
        }
    }

    function setFreePlanetPass (uint _planetId, bool _freePass) external isPlanetPlotHandler() {
        require (planetSide[_planetId] != 0, "Planet does not exist");
        freePlanetPass[_planetId] = _freePass;
    }

    function setPlayerPlanetPass (address _renter, uint[] memory _planetIds) external isPlanetPlotHandler() {
        for (uint256 index = 0; index < _planetIds.length; index++) {
            require (planetSide[_planetIds[index]] != 0, "Planet does not exist");
            planetPass[_renter][_planetIds[index]] = true;
        }
    }

    function setPlotOpen (address _owner, uint _plotId, bool _open) external isPlanetPlotHandler() {
        require(_owner == ownerOf(_plotId), "_owner and _plotId owner does not match");
        plotState[_plotId].open = _open;
    }

    function setPlotRent (address _owner, uint _plotId, uint _rent) external isPlanetPlotHandler() {
        require (_rent >= 0);
        require(_owner == ownerOf(_plotId), "_owner and _plotId owner does not match");
        plotState[_plotId].rent = _rent;
    }

    function replenishPlot (uint _plotId, uint _amount) external isPlanetPlotHandler() returns (bool) {
        return _replenishPlot(_plotId, _amount);
    }

    function _replenishPlot (uint _plotId, uint _amount) internal returns (bool) {
        require(_exists(_plotId), "Query for nonexistent plot");
        uint dc = plotState[_plotId].left + _amount;
        if (dc >= plotState[_plotId].max) {
            plotState[_plotId].left = plotState[_plotId].max;
        } else {
            plotState[_plotId].left = dc;
        }
        return true;
    }

    function upgradePlotMax (uint _plotId, uint _newMax) external isPlanetPlotHandler() returns (bool) {
        require(_exists(_plotId), "Query for nonexistent plot");
        require(_newMax >= 0);
        plotState[_plotId].max = _newMax;
        plotState[_plotId].left = _newMax;
        return true;
    }

    function updateAutoReplenish (uint _digsPerTimeUnit, uint _replenishTimeUnit) external isPlanetPlotHandler() {
        digsPerTimeUnit = _digsPerTimeUnit;
        replenishTimeUnit = _replenishTimeUnit;
    }

    function rentInfo(uint _plotId) external view returns (address, uint, uint, bool){
        return (ownerOf(_plotId), plotState[_plotId].left, plotState[_plotId].rent, plotState[_plotId].open);
    }

    function plotMax(uint _plotId) external view returns (uint){
        return(plotState[_plotId].max);
    }

    function planetExists(uint _planetId) external view returns (bool){
        if(planetSide[_planetId] > 0){
            return true;
        }else{
            return false;
        }
    }

    function testnetPassGranter(address[] memory _addresses, uint _planet) public {///////////////////////////////////////////////////////////////////////////////////////////REMOVE
        require(hasRole(DEFAULT_ADMIN_ROLE, msg.sender),"Tx not from admin");
        require (planetSide[_planet] != 0, "Planet does not exist");
        for (uint256 index = 0; index < _addresses.length; index++) {
            planetPass[_addresses[index]][_planet] = true;
        }
    }

    function tokenURI(uint _plotId) public view override returns (string memory) {
        require(_exists(_plotId), "ERC721Metadata: URI query for nonexistent token");
        return IChromiaNetResolver(CNR).getNFTURI(address(this), _plotId);
    }
}