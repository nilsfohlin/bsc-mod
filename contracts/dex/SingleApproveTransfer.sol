// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/interfaces/IERC1155.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract SingleApproveTransfer is AccessControl{

    bytes32 public constant FACTORY = keccak256("FACTORY");
    bytes32 public constant PAIR = keccak256("PAIR");

    IERC20 public dar; 
    address public resources; 

    constructor (IERC20 _dar, address _resources) {
        dar = _dar;
        resources = _resources;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    modifier isPair(){
        require(hasRole(PAIR, msg.sender),"Not PAIR");
        _;
    }

    function takeDAR(address _sender, uint _darAmount) external isPair(){
        IERC20(dar).transferFrom(_sender, msg.sender, _darAmount);
    }

    function takeResource(address _sender, uint _resourceId, uint _resourceAmount) external isPair(){
        IERC1155(resources).safeTransferFrom(_sender, msg.sender, _resourceId, _resourceAmount, "");
    }

    function setPair(address _pair) external{
        require(hasRole(FACTORY, msg.sender),"Not FACTORY");
        _grantRole(PAIR, _pair);
    }
}