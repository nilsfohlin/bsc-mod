// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "interfaces/IResources.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/interfaces/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "interfaces/ISingleApproveTransfer.sol";


contract Pair is ERC1155Holder, AccessControl{

    bytes32 public constant FACTORY = keccak256("FACTORY");
    bytes32 public constant LIQUIDITY_PROVIDER = keccak256("LIQUIDITY_PROVIDER");

    IERC20 public dar; 
    address public resources;
    address public singleApproveTransfer; 

    uint public resourceId;

    uint public kMultiplier;


    constructor () {
        _setupRole(FACTORY, msg.sender);
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC1155Receiver, AccessControl) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    modifier isLiquidityProvider(){
        require(hasRole(LIQUIDITY_PROVIDER, msg.sender),"Not LIQUIDITY_PROVIDER");
        _;
    }

    function init(IERC20 _dar, address _resources, uint _resourceId, address _singleApproveTransfer, address _owner) external {
        require(hasRole(FACTORY, msg.sender),"Not FACTORY");
        dar = _dar;
        resources = _resources;
        singleApproveTransfer = _singleApproveTransfer;
        require(IResources(resources).getResourceCount() > _resourceId, "Resource does not exist");
        resourceId = _resourceId;
        _setupRole(DEFAULT_ADMIN_ROLE, _owner);
    }

    //DEV
    function k() public view returns (uint){
        return IERC20(dar).balanceOf(address(this)) * IERC1155(resources).balanceOf(address(this), resourceId);
    }
    //DEV
    function price() public view returns (uint){
        return IERC20(dar).balanceOf(address(this)) / IERC1155(resources).balanceOf(address(this), resourceId);
    }

    function addLiquidity(uint _darAmount, uint _resourceAmount) public isLiquidityProvider(){
        uint[] memory resource = new uint[](1);
        resource[0] = resourceId;
        uint[] memory amount = new uint[](1);

        if (IERC20(dar).balanceOf(address(this)) != 0){
            amount[0] = (_darAmount * IERC1155(resources).balanceOf(address(this), resourceId)) / IERC20(dar).balanceOf(address(this));
        } else {
            amount[0] = _resourceAmount;
        }

        IERC20(dar).transferFrom(msg.sender, address(this), _darAmount);
        IResources(resources).mintBatch(address(this), resource, amount);

        kMultiplier = IERC20(dar).balanceOf(address(this)) * IERC1155(resources).balanceOf(address(this), resourceId);
    }

    function removeLiquidity(uint _percent) public isLiquidityProvider(){
        uint amountDarToRemove;
        uint[] memory amount = new uint[](1);

        if (_percent < 100)
        {
            amountDarToRemove = (IERC20(dar).balanceOf(address(this)) * _percent) / 100;
            amount[0] = (IERC1155(resources).balanceOf(address(this), resourceId) * _percent) / 100;
        } else {
            amountDarToRemove = IERC20(dar).balanceOf(address(this));
            amount[0] = IERC1155(resources).balanceOf(address(this), resourceId);
        }

        IERC20(dar).transfer(msg.sender, amountDarToRemove);

        uint[] memory resource = new uint[](1);
        resource[0] = resourceId;

        IResources(resources).burnBatch(address(this), resource, amount);

        kMultiplier = IERC20(dar).balanceOf(address(this)) * IERC1155(resources).balanceOf(address(this), resourceId);
    }

    function swap(uint _resourceIn, uint _resourceOut) public {
        if(_resourceOut > 0){
            uint darToPay = (kMultiplier / (IERC1155(resources).balanceOf(address(this), resourceId) - _resourceOut)) - IERC20(dar).balanceOf(address(this));

            require (darToPay > 0, "Not enough Liquidity");

            ISingleApproveTransfer(singleApproveTransfer).takeDAR(msg.sender, darToPay);
            IERC1155(resources).safeTransferFrom(address(this), msg.sender, resourceId, _resourceOut, "");
            
        }else if(_resourceIn > 0){
            uint darToGet = IERC20(dar).balanceOf(address(this)) - (kMultiplier / (IERC1155(resources).balanceOf(address(this), resourceId) + _resourceIn));

            require (darToGet > 0, "Not enough Liquidity");

            ISingleApproveTransfer(singleApproveTransfer).takeResource(msg.sender, resourceId, _resourceIn);
            IERC20(dar).transfer(msg.sender, darToGet);
        }
    }
}
    

