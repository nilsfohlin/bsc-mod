// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "interfaces/ISingleApproveTransfer.sol";
import "interfaces/IPair.sol";
import "./Pair.sol";

contract Factory is AccessControl{

    bytes32 public constant FACTORY_HANDLER = keccak256("FACTORY_HANDLER");

    IERC20 public dar; 
    address public resources; 
    address public singleApproveTransfer;

    event PairCreated (address pair, uint resourceId);

    mapping (uint => address) public getPair;

    constructor (IERC20 _dar, address _resources, address _singleApproveTransfer) {
        dar = _dar;
        resources = _resources;
        singleApproveTransfer = _singleApproveTransfer;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    function createPair(uint _resourceId) public returns (address pair){
        require(hasRole(FACTORY_HANDLER, msg.sender),"Not FACTORY_HANDLER");
        require(getPair[_resourceId] == address(0), "Pair already exists");

        bytes memory bytecode = type(Pair).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(_resourceId));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        IPair(pair).init(dar, resources, _resourceId, singleApproveTransfer, msg.sender);
        ISingleApproveTransfer(singleApproveTransfer).setPair(pair);

        getPair[_resourceId] = pair;

        emit PairCreated(pair, _resourceId);
    }
}