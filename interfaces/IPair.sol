// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IPair {
    function init(IERC20 _dar, address _resources, uint _resourceId, address _transferer, address _owner) external;
}