// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface IItems {
    function createItemType(uint _type) external returns (bool);
    function mint(address _to, uint256[] memory _mintTypes) external  returns (bool);
    function burn(address _from, uint256[] memory _burnItemIds) external  returns (bool);
    function burnAndMint(address _fromTo, uint256[] memory _mintTypes, uint256[] memory _burnItemIds) external returns (bool);
    function prefixExists(uint _prefix) external view returns (bool);
}