// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface IResources {
    function mintBatch(address _to, uint[] memory _resources, uint[] memory _amounts) external returns (bool);
    function createResource(string memory _resourceName) external;
    function burnBatch(address _from, uint256[] memory _ids, uint256[] memory _amounts) external returns (bool);
    function getResourceCount() external view returns (uint);
}