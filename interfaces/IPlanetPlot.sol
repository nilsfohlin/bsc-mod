// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface IPlanetPlot {
    function mintPlotRegion(address _to, uint _planetId, uint _region) external;
    function createPlanet (uint _xy, uint _id) external;
    function openRentPlot(address _renter, uint _plotId, uint8 _digsToOpen) external returns (bool);
    function setFreePlanetPass (uint _planetId, bool _freePass) external;
    function setPlayerPlanetPass (address _renter, uint[] memory _planetIds) external;
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function rentInfo(uint _plotId) external view returns (address, uint, uint, bool);
    function setPlotOpen (address _owner, uint _plotId, bool _open) external;
    function setPlotRent (address _owner, uint _plotId, uint _rent) external;
    function replenishPlot (uint _plotId, uint _digs) external returns (bool);
    function upgradePlotMax (uint _plotId, uint _newMax) external returns (bool);
    function plotMax(uint _plotId) external view returns (uint);
    function planetExists(uint _planetId) external view returns (bool);
    function updateAutoReplenish (uint _digsPerTimeUnit, uint _replenishTimeUnit) external;
}