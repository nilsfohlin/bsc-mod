// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface ISingleApproveTransfer{
    function takeDAR(address _sender, uint _darAmount) external;
    function takeResource(address _sender, uint _resourceId, uint _resourceAmount) external;
    function setPair(address _pair) external;
}
