// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface IPlanetPlotHandler {
    function closeRentPlot(address _renter, uint _nrToClose) external returns (bool);
    function replenishPlot(uint _tokenId, uint _digs) external returns (bool);
    function upgradePlotMax(uint _tokenId, uint _newMax) external returns (bool);
    function setPlayerPlanetPass(address _renter, uint[] memory _planetIds) external;
}