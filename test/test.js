const { expect } = require("chai");
const { ethers } = require("hardhat");
const help = require("./test_helper_functions.js");

describe("Test create planet and mint plot", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    let resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
  });

  it("Test create planet", async function () {
    const [owner, creator] = await ethers.getSigners();

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();

    await expect(planetPlotHandler.connect(creator).createPlanet(10, 100)).to.be.revertedWith('PLANET_PLOT_CREATOR ROLE required');
    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await expect(planetPlotHandler.connect(creator).createPlanet(10, 100)).to.be.revertedWith('Tx not from planetPlotHandler');
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);

    await expect(planetPlotHandler.connect(creator).createPlanet(10, 1000)).to.be.revertedWith('planet id must be between 100 and 1000');
    await expect(planetPlotHandler.connect(creator).createPlanet(10, 10)).to.be.revertedWith('planet id must be between 100 and 1000');

    await expect(planetPlotHandler.connect(creator).createPlanet(9, 100)).to.be.revertedWith('_sideLength must be greater than 0 and divisible by 100 and less than 310');
    await expect(planetPlotHandler.connect(creator).createPlanet(320, 100)).to.be.revertedWith('_sideLength must be greater than 0 and divisible by 100 and less than 310');

    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await expect(planetPlotHandler.connect(creator).createPlanet(10, 100)).to.be.revertedWith('Planet id taken');

    expect(await planetPlot.planetSide(100)).to.equal(10);
  });

  it("Test mint plot region", async function () {
    const [owner, creator, plotOwner] = await ethers.getSigners();

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();

    await expect(planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 4)).to.be.revertedWith('PLANET_PLOT_CREATOR ROLE required');
    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await expect(planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 4)).to.be.revertedWith('Tx not from planetPlotHandler');
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);

    await planetPlotHandler.connect(creator).createPlanet(40, 100);

    await expect(planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 200, 4)).to.be.revertedWith('PlanetId does not exist');

    await expect(planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 0)).to.be.revertedWith('Invalid region');
    await expect(planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 17)).to.be.revertedWith('Invalid region');

    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);

    expect(await planetPlot.ownerOf(100001001001)).to.be.equal(plotOwner.address);
    expect(await planetPlot.ownerOf(100001005005)).to.be.equal(plotOwner.address);
    expect(await planetPlot.ownerOf(100001010010)).to.be.equal(plotOwner.address);
    await expect(planetPlot.ownerOf(100001010011)).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(planetPlot.ownerOf(100001000000)).to.be.revertedWith('ERC721: owner query for nonexistent token');

    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 16);

    expect(await planetPlot.ownerOf(100016032032)).to.be.equal(plotOwner.address);
    expect(await planetPlot.ownerOf(100016035035)).to.be.equal(plotOwner.address);
    expect(await planetPlot.ownerOf(100016040040)).to.be.equal(plotOwner.address);
    await expect(planetPlot.ownerOf(100016040041)).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(planetPlot.ownerOf(100016030030)).to.be.revertedWith('ERC721: owner query for nonexistent token');

    await expect(planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1)).to.be.revertedWith('ERC721: token already minted');
  });

  it("Test create 'max' planet", async function () {
    const [owner, creator, plotOwner] = await ethers.getSigners();

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();

    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);

    await planetPlotHandler.connect(creator).createPlanet(310, 100);

    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);

    expect(await planetPlot.ownerOf(100001001001)).to.be.equal(plotOwner.address);
    await expect(planetPlot.ownerOf(100001010011)).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(planetPlot.ownerOf(100001000000)).to.be.revertedWith('ERC721: owner query for nonexistent token');

    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 961);

    expect(await planetPlot.ownerOf(100961310310)).to.be.equal(plotOwner.address);
    await expect(planetPlot.ownerOf(100961310311)).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(planetPlot.ownerOf(100961299299)).to.be.revertedWith('ERC721: owner query for nonexistent token');
  });
});

describe("Test create Resources and Items", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);
  });

  it("Test create resource", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, resourceItemCreator] = await ethers.getSigners();
    
    await expect(resourcesHandler.connect(resourceItemCreator).createResource('GOLD')).to.be.revertedWith('RESOURCE_ITEM_CREATOR required');

    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);

    await expect(resourcesHandler.connect(resourceItemCreator).createResource('GOLD')).to.be.revertedWith('Tx not from resourcesHandler');

    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);

    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    expect(await resources.resourceCatalogue(0)).to.be.equal('GOLD');

    await expect(resourcesHandler.connect(resourceItemCreator).createResource('GOLD')).to.be.revertedWith('Resource name already taken');

    await resourcesHandler.connect(resourceItemCreator).createResource('gold');
    expect(await resources.resourceCatalogue(1)).to.be.equal('gold');

  });

  it("Test create item", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, resourceItemCreator] = await ethers.getSigners();

    await expect(resourcesHandler.connect(resourceItemCreator).createItem(1000001000)).to.be.revertedWith('RESOURCE_ITEM_CREATOR required');

    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);

    await expect(resourcesHandler.connect(resourceItemCreator).createItem(1000001000)).to.be.revertedWith('Tx not from resourcesHandler');

    var RESOURCES_HANDLER = await items.RESOURCES_HANDLER();
    await items.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);

    await expect(resourcesHandler.connect(resourceItemCreator).createItem(10000000001)).to.be.revertedWith('Invalid prefix');
    await expect(resourcesHandler.connect(resourceItemCreator).createItem(1000000999)).to.be.revertedWith('Invalid prefix');

    await resourcesHandler.connect(resourceItemCreator).createItem(1000001000);
    expect(await items.typeCount(1000001000)).to.be.equal(1);

    await expect(resourcesHandler.connect(resourceItemCreator).createItem(1000001000)).to.be.revertedWith('Prefix taken');

    await resourcesHandler.connect(resourceItemCreator).createItem(1000001001);
    expect(await items.typeCount(1000001001)).to.be.equal(1);
  });
});

describe("Test open plot rent without pass", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    let resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
  });

  it("Rent plot default settings", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy] = await ethers.getSigners();
    //setup
    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);

    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 100)).to.be.revertedWith('An address can not open this many digs in on tx');
    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.be.revertedWith('ERC20: transfer amount exceeds allowance');

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.be.revertedWith('Renter does not have a valid pass for this planet');

    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);

    await expect(planetPlotHandler.connect(gameEconomy).setFreePlanetPass(102, true)).to.be.revertedWith('Planet does not exist');

    await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);
    
    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 5)).to.emit(planetPlotHandler, 'Rent').withArgs(player.address, plotOwner.address, 100001001001, 5); // pay for five digs

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000 - 5000000); // DAR has 6 decimals, rent is 1 (000 000) dar, faucet gives 100 (000 000) dar
    expect(await testDAR.balanceOf(plotOwner.address)).to.be.equal((100000000 + 5000000) - 500000); // total rent 5 (000 000) dar tax is 10 %
    expect(await testDAR.balanceOf(taxAccount.address)).to.be.equal(100000000 + 500000); // 10 % tax 

    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);
    //maximum 10 digs open at once
    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.be.revertedWith('Address can not open this amount of digs until closing previous digs');
  });

  it("Rent plot change setting by admin", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy] = await ethers.getSigners();
    //setup
    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);

    await expect(planetPlotHandler.connect(gameEconomy).updateTaxAccount(server.address)).to.be.revertedWith('GAME_CONTROL required');
    await expect(planetPlotHandler.connect(gameEconomy).updateTaxRate(50)).to.be.revertedWith('GAME_CONTROL required');
    await expect(planetPlotHandler.connect(gameEconomy).updateFixedRent(100000)).to.be.revertedWith('GAME_CONTROL required');
    await expect(planetPlotHandler.connect(gameEconomy).updateDigLimits(101, 101)).to.be.revertedWith('GAME_CONTROL required');

    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);

    await planetPlotHandler.connect(gameEconomy).updateTaxAccount(server.address);
    await planetPlotHandler.connect(gameEconomy).updateTaxRate(50); // 50%
    await planetPlotHandler.connect(gameEconomy).updateFixedRent(100000); // 0.1 dar
    await planetPlotHandler.connect(gameEconomy).updateDigLimits(101, 101);
    await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);
    
    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 100)).to.emit(planetPlotHandler, 'Rent').withArgs(player.address, plotOwner.address, 100001001001, 100); // pay for 100 digs

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000 - 10000000); // DAR has 6 decimals, rent is 0.1 (000 000) dar, faucet gives 100 (000 000) dar
    expect(await testDAR.balanceOf(plotOwner.address)).to.be.equal((100000000 + 10000000) - 5000000); // total rent 10 (000 000) dar tax is 50 %
    expect(await testDAR.balanceOf(taxAccount.address)).to.be.equal(100000000); // changed tax account 
    expect(await testDAR.balanceOf(server.address)).to.be.equal(100000000 + 5000000); // 50 % tax 

    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.be.revertedWith('This amount of digs not available on plot');
  });

  it("Rent plot change setting by plotOwner", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy] = await ethers.getSigners();
    //setup
    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);
    await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);

    await expect(planetPlotHandler.connect(player).setPlotRent(100001001001, 2000000)).to.be.revertedWith('_owner and _plotId owner does not match');
    await expect(planetPlotHandler.connect(player).setPlotOpen(100001001001, 2000000)).to.be.revertedWith('_owner and _plotId owner does not match');

    await planetPlotHandler.connect(plotOwner).setPlotRent(100001001001, 2000000);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await expect(await planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent').withArgs(player.address, plotOwner.address, 100001001001, 1);

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000 - 2000000); // DAR has 6 decimals, rent is 2 (000 000) dar, faucet gives 100 (000 000) dar
    expect(await testDAR.balanceOf(plotOwner.address)).to.be.equal((100000000 + 2000000) - 200000); // total rent 10 (000 000) dar tax is 10 %
    expect(await testDAR.balanceOf(taxAccount.address)).to.be.equal(100000000 + 200000);

    await planetPlotHandler.connect(plotOwner).setPlotOpen(100001001001, false); //make it so only owner can rent from themselfs

    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.be.revertedWith('Plot owner is not allowing rents at this time');
    await expect(await planetPlotHandler.connect(plotOwner).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent').withArgs(plotOwner.address, plotOwner.address, 100001001001, 1);

    //unchanged from before since owner does not pay rent to themself
    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000 - 2000000); 
    expect(await testDAR.balanceOf(plotOwner.address)).to.be.equal((100000000 + 2000000) - 200000);
    expect(await testDAR.balanceOf(taxAccount.address)).to.be.equal(100000000 + 200000);
  });

  it("Test paused works", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, gamePauser] = await ethers.getSigners();
    //setup
    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, gamePauser.address);
    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);
    await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);

    await expect(planetPlotHandler.connect(player).pauseHandler()).to.be.revertedWith('GAME_PAUSER required');
    await planetPlotHandler.connect(gamePauser).pauseHandler();
    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.be.revertedWith('Pausable: paused');
    await expect(planetPlotHandler.connect(player).unpauseHandler()).to.be.revertedWith('GAME_PAUSER required');
    await planetPlotHandler.connect(gamePauser).unpauseHandler();
    await expect(await planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent').withArgs(player.address, plotOwner.address, 100001001001, 1);
  });
});

describe("Test close plot rent", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();

    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
    await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');
    await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);

  });

  it("Test close rent setup", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator, gameControl] = await ethers.getSigners();

    await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent').withArgs(player.address, plotOwner.address, 100001001001, 1);
    expect(await planetPlotHandler.addressIsRenting(player.address)).to.be.equal(1);

    var digsToClose = await planetPlotHandler.addressIsRenting(player.address);
    var resourcesToMint = [0,1,2];
    var amountsToMint = [10,10,10];
    var internalNonce = await planetPlotHandler.internalNonce(player.address);

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

    await expect(planetPlotHandler.connect(gameControl).updateServer(server.address)).to.be.revertedWith('GAME_CONTROL required');
   
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);

    await planetPlotHandler.connect(gameControl).updateServer(server.address);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Tx not from resourcesHandler');

    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);

    await expect(await planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.emit(planetPlotHandler, 'CloseRentAndMint').withArgs(player.address, 1);

    expect(await planetPlotHandler.addressIsRenting(player.address)).to.be.equal(0);

    expect(await resources.balanceOf(player.address, 0)).to.be.equal(10);
    expect(await resources.balanceOf(player.address, 1)).to.be.equal(10);
    expect(await resources.balanceOf(player.address, 2)).to.be.equal(10);
  });

  it("Test close rent signature", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator, gameControl] = await ethers.getSigners();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);
    var RESOURCES_HANDLER = await planetPlotHandler.RESOURCES_HANDLER();
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);

    // lets open five digs

    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);

    var digsToClose = await planetPlotHandler.addressIsRenting(player.address);
    var resourcesToMint = [0,1,2];
    var amountsToMint = [10,10,10];
    var internalNonce = await planetPlotHandler.internalNonce(player.address);

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);

    // to close = 5
    // nonce = 0
    // ids = [0,1,2]
    // amounts = [10,10,10]

    // lets sign with not server key

    var serverSig = await player.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

    // lets sign with correct key and change each input and send to contract for check
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      creator.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, [1], amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, [1000,1000,1000], digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, 1, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

    // lets send valid tx and then send the same again anfd try to "double mint"

    await expect(await planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.emit(planetPlotHandler, 'CloseRentAndMint').withArgs(player.address, 5);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Signature invalid');

  });

  it("Test close rent logics", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator, gameControl] = await ethers.getSigners();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);
    var RESOURCES_HANDLER = await planetPlotHandler.RESOURCES_HANDLER();
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);

    // lets try to send a valid signture from server but the player has not rented
    // this is what we have not ran ( await planetPlotHandler.connect(player).openRentPlot(100001001001, 1); )
    // we do this in order to try to mint stuff without playing

    var digsToClose = 0;
    var resourcesToMint = [0,1,2];
    var amountsToMint = [10,10,10];
    var internalNonce = await planetPlotHandler.internalNonce(player.address);

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Can not close 0 digs');

    // lets say we manage to get a signature that says we have opened a dig but we have not

    var digsToClose = 1;

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Address does not have this many rents open');

    // lest say we "pay" for one dig and manage to get a signature that says we have two

    await planetPlotHandler.connect(player).openRentPlot(100001001001, 1);

    var digsToClose = 2;

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Address does not have this many rents open');
  });

  it("Test close rent pause", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator, gameControl, pauser] = await ethers.getSigners();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);
    var RESOURCES_HANDLER = await planetPlotHandler.RESOURCES_HANDLER();
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);

    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);

    var digsToClose = await planetPlotHandler.addressIsRenting(player.address);
    var resourcesToMint = [0,1,2];
    var amountsToMint = [10,10,10];
    var internalNonce = await planetPlotHandler.internalNonce(player.address);

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(resourcesHandler.connect(pauser).pauseHandler()).to.be.revertedWith('GAME_PAUSER required');
    await expect(planetPlotHandler.connect(pauser).pauseHandler()).to.be.revertedWith('GAME_PAUSER required');
    await expect(resourcesHandler.connect(pauser).unpauseHandler()).to.be.revertedWith('GAME_PAUSER required');
    await expect(planetPlotHandler.connect(pauser).unpauseHandler()).to.be.revertedWith('GAME_PAUSER required');

    var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
    await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
    await resourcesHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);

    // pause planetPlot Handler but not resourceHandler
    await planetPlotHandler.connect(pauser).pauseHandler()

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Pausable: paused');

    await planetPlotHandler.connect(pauser).unpauseHandler()

    await expect(await planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.emit(planetPlotHandler, 'CloseRentAndMint').withArgs(player.address, 5);

  });

  it("Test close rent and mint resources that do not exist", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator, gameControl, pauser] = await ethers.getSigners();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);

    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);

    var digsToClose = await planetPlotHandler.addressIsRenting(player.address);
    var resourcesToMint = [3,4,5];
    var amountsToMint = [10,10,10];
    var internalNonce = await planetPlotHandler.internalNonce(player.address);

    var obj = ethers.utils.defaultAbiCoder.encode(
      [ "address", "uint[]", "uint[]", "uint", "uint" ], 
      [player.address, resourcesToMint, amountsToMint, digsToClose, internalNonce]);

    obj = ethers.utils.arrayify(obj);
    var prefix = ethers.utils.toUtf8Bytes("\x19Ethereum Signed Message:\n" + obj.length);
    var serverSig = await server.signMessage(obj);
    var {v: v1, r: r1, s: s1 } = ethers.utils.splitSignature(serverSig);

    await expect(planetPlotHandler.connect(player).closeRentAndMint(
      player.address, resourcesToMint, amountsToMint, digsToClose, prefix, v1, r1, s1
    )).to.be.revertedWith('Trying to mint non-existant resource');
  });
});

describe("Test craftingRecipes", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount, resourceItemCreator, gameControl, pauser] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();

    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);

    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await items.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
    
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
    await resourcesHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);

    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
    await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');

    await resourcesHandler.connect(resourceItemCreator).createItem(1000001001);
    await resourcesHandler.connect(resourceItemCreator).createItem(1000001002);
    await resourcesHandler.connect(resourceItemCreator).createItem(1234561234);
    await resourcesHandler.connect(resourceItemCreator).createItem(6543214321);
  });
   
  it("Test craftingRecipe creation", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
       resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [0], [1], [], 0, [1,2], [10,10], []))
    .to.be.revertedWith('RECIPE_CREATOR required');

    var RECIPE_CREATOR = await resourcesHandler.RECIPE_CREATOR();
    await resourcesHandler.connect(owner).grantRole(RECIPE_CREATOR, recipeCreator.address);

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 0, [1,2], [10,10], []))
    .to.be.revertedWith('Must have 1 input+');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [], 0, [], [], []))
    .to.be.revertedWith('Must have 1 output+');

    //RESOURCES INPUT TEST
    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [500], [1], [], 0, [1], [1], []))
    .to.be.revertedWith('Resource not exist');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1,2], [1], [], 0, [1], [1], []))
    .to.be.revertedWith('Id/pre arr not same length as amount arr');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1,1], [], 0, [1], [1], []))
    .to.be.revertedWith('Id/pre arr not same length as amount arr');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [0], [], 0, [1], [1], []))
    .to.be.revertedWith('Cant be zero amount');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1,2,3,1], [1,1,1,1], [], 0, [1], [1], []))
    .to.be.revertedWith('Can not have the same _rId input multiple times');

    //ITEMS INPUT TEST
    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [1000009999], 0, [1], [1], []))
    .to.be.revertedWith('Item not exist');

    //RESOURCE INPUT TEST - this is not nessecary really
    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [], 0, [5], [1], []))
    .to.be.revertedWith('Resource not exist');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [], 0, [1,2], [1], []))
    .to.be.revertedWith('Id/pre arr not same length as amount arr');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [], 0, [1], [1,1], []))
    .to.be.revertedWith('Id/pre arr not same length as amount arr');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [], 0, [1], [0], []))
    .to.be.revertedWith('Cant be zero amount');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [], 0, [1,2,3,1], [1,1,1,1], []))
    .to.be.revertedWith('Can not have the same _rId input multiple times');

    //ITEMS OUTPUT TEST - this is not nessecary really
    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1, [], [], [1000001001, 1000009999]))
    .to.be.revertedWith('Item not exist');

    // ACTUALLY ADD A RECIPE
    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [1], [1], [1000001001,1000001001], 1, [2], [2], [1000001002]))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 1, true);

    // RECIPE ID TAKEN
    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1, [1], [1], []))
    .to.be.revertedWith('Recipe id taken');
  });

  it("Test craftingRecipe removal", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
       resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    var RECIPE_CREATOR = await resourcesHandler.RECIPE_CREATOR();
    await resourcesHandler.connect(owner).grantRole(RECIPE_CREATOR, recipeCreator.address);

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1, [1], [1], []))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 1, true);

    expect(await resourcesHandler.recipeExists(1, 1)).to.be.equal(true);

    await expect(resourcesHandler.connect(player).removeCraftingRecipe(1))
    .to.be.revertedWith('RECIPE_CREATOR required');

    await expect(resourcesHandler.connect(recipeCreator).removeCraftingRecipe(1))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 1, false);

    expect(await resourcesHandler.recipeExists(1, 1)).to.be.equal(false);
  });
});

describe("Test crafting", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
    var RECIPE_CREATOR = await resourcesHandler.RECIPE_CREATOR();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();

    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);

    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await items.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
    
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
    await resourcesHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);

    await resourcesHandler.connect(owner).grantRole(RECIPE_CREATOR, recipeCreator.address);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);

    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
    await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');

    await resourcesHandler.connect(resourceItemCreator).createItem(1000001001);
    await resourcesHandler.connect(resourceItemCreator).createItem(1000001002);
    await resourcesHandler.connect(resourceItemCreator).createItem(1234561234);
    await resourcesHandler.connect(resourceItemCreator).createItem(6543214321);
  });

  it("Test craft", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
       resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await expect(resourcesHandler.connect(player).craft(1,[])).to.be.revertedWith('Recipe id not exist');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1000000, [0,1,2], [10,10,10], [1000001001, 1000001001, 1000001001, 1000001001, 1000001001, 1000001001]))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 1, true);

    await testDAR.connect(player).approve(resourcesHandler.address, 1000000000);
    await testDAR.connect(owner).approve(resourcesHandler.address, 1000000000);

    await expect(resourcesHandler.connect(player).craft(1, []))
    .to.emit(resourcesHandler, 'CraftEvent').withArgs(player.address, 1);

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000 - 1000000);
    expect(await testDAR.balanceOf(taxAccount.address)).to.be.equal(100000000 + 1000000);
    expect(await resources.balanceOf(player.address, 0)).to.be.equal(10);
    expect(await resources.balanceOf(player.address, 1)).to.be.equal(10);
    expect(await resources.balanceOf(player.address, 2)).to.be.equal(10);

    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000005"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000006"))).to.be.equal(player.address);

    await expect(resourcesHandler.connect(owner).craft(1, []))
    .to.emit(resourcesHandler, 'CraftEvent').withArgs(owner.address, 1);

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(2, [0,1,2], [10,10,10], [1000001001,1000001001,1000001001,1000001001], 0, [], [], [1000001002]))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(2, 1, true);

    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004")
    ])).to.be.revertedWith('ERC1155: caller is not owner nor approved');

    await resources.connect(player).setApprovalForAll(resourcesHandler.address, player.address);

    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004")
    ])).to.be.revertedWith('ERC721: caller is not owner nor approved');

    await items.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
    await items.connect(owner).setApprovalForAll(resourcesHandler.address, owner.address);

    //emtpy array
    await expect(resourcesHandler.connect(player).craft(2, []))
      .to.be.revertedWith('Recipe - tokenIds arr no match');
    
    // too short
    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
    ])).to.be.revertedWith('Recipe - tokenIds arr no match');

    //too long
    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000005")
    ])).to.be.revertedWith('Recipe - tokenIds arr no match');

    //same tokens multiple times
    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"),
    ])).to.be.revertedWith('ERC721: operator query for nonexistent token');

    //not same as recipe
    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
      ethers.BigNumber.from("100000100300000000000000000000000000000000000000000000000000000000000000000004"),
    ])).to.be.revertedWith('Token, recipe prefix mismatch');

    // to short token id (last one)
    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
      ethers.BigNumber.from("10000010010000000000000000000000000000000000000000000000000000000000000000004"),
    ])).to.be.revertedWith('Token, recipe prefix mismatch');

    // sending another users token as your own
    await expect(resourcesHandler.connect(player).craft(2, 
      [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000007"),
    ])).to.be.revertedWith('Not owner of item input');

    await expect(resourcesHandler.connect(player).craft(2, 
    [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
    ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
    ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
    ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004")
  ])).to.emit(items, 'Craft').withArgs(player.address,
    [ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"),
    ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"),
    ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"),
    ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004")],
    [ethers.BigNumber.from("100000100200000000000000000000000000000000000000000000000000000000000000000001")]
    );

    await expect(items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000001"))).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000002"))).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000003"))).to.be.revertedWith('ERC721: owner query for nonexistent token');
    await expect(items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000004"))).to.be.revertedWith('ERC721: owner query for nonexistent token');
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000005"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000006"))).to.be.equal(player.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100200000000000000000000000000000000000000000000000000000000000000000001"))).to.be.equal(player.address);

    expect(await resources.balanceOf(player.address, 0)).to.be.equal(0);
    expect(await resources.balanceOf(player.address, 1)).to.be.equal(0);
    expect(await resources.balanceOf(player.address, 2)).to.be.equal(0);

    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000007"))).to.be.equal(owner.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000008"))).to.be.equal(owner.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000009"))).to.be.equal(owner.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000010"))).to.be.equal(owner.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000011"))).to.be.equal(owner.address);
    expect(await items.ownerOf(ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000012"))).to.be.equal(owner.address);

    expect(await resources.balanceOf(owner.address, 0)).to.be.equal(10);
    expect(await resources.balanceOf(owner.address, 1)).to.be.equal(10);
    expect(await resources.balanceOf(owner.address, 2)).to.be.equal(10);

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(3, [], [], [1000001001,1000001002], 0, [], [], [1234561234]))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(3, 1, true);

    await expect(resourcesHandler.connect(player).craft(3, 
      [ethers.BigNumber.from("100000100200000000000000000000000000000000000000000000000000000000000000000001"),
      ethers.BigNumber.from("100000100100000000000000000000000000000000000000000000000000000000000000000005")
    ])).to.be.revertedWith('Token, recipe prefix mismatch');

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(4, [0], [1], [], 0, [], [], [1234561234]))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(4, 1, true);

    await expect(resourcesHandler.connect(player).craft(4, [])).to.be.revertedWith('ERC1155: burn amount exceeds balance');
  });

  it("Test pause", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
       resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await expect(resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1000000, [0,1,2], [10,10,10], [1000001001, 1000001001, 1000001001, 1000001001, 1000001001, 1000001001]))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 1, true);
    await testDAR.connect(player).approve(resourcesHandler.address, 1000000000);

    await resourcesHandler.connect(pauser).pauseHandler();

    await expect(resourcesHandler.connect(player).craft(1, []))
    .to.be.revertedWith('Pausable: paused');
  });
});

describe("Test plotRURecipe", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
    var RECIPE_CREATOR = await resourcesHandler.RECIPE_CREATOR();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();

    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);

    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);

    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await items.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
    await resourcesHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);

    await resourcesHandler.connect(owner).grantRole(RECIPE_CREATOR, recipeCreator.address);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);

    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
    await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');
  });

  it("Test add plotRURecipe", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await expect(resourcesHandler.connect(player).addPlotRURecipe(1,[1],[1],1,100,20))
      .to.be.revertedWith('RECIPE_CREATOR required');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1,[1],[1],1,100,0))
      .to.be.revertedWith('_add must be greater than zero');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1,[1],[1],1,100,-1))
      .to.be.reverted;
    
    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [], [], 0, 100, 10))
      .to.be.revertedWith('Must have 1 input+');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [4], [1], 0, 100, 10))
      .to.be.revertedWith('Resource not exist');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [2,2], [1,1], 0, 100, 10))
      .to.be.revertedWith('Can not have the same _rId input multiple times');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [2], [1,1], 0, 100, 10))
      .to.be.revertedWith('Id/pre arr not same length as amount arr');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1,2], [1], 0, 100, 10))
      .to.be.revertedWith('Id/pre arr not same length as amount arr');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1,2], [0,0], 0, 100, 10))
      .to.be.revertedWith('Cant be zero amount');

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1,2], [1,1], 1, 100, 10))
      .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 2, true);

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1,2], [0,0], 0, 100, 10))
      .to.be.revertedWith('recipe id taken');
  });

  it("Test remove plotRURecipe", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1,2], [1,1], 1, 100, 10))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 2, true);

    expect(await resourcesHandler.recipeExists(1,2)).to.be.equal(true);
    
    await expect(resourcesHandler.connect(player).removePlotRURecipe(1))
    .to.be.revertedWith('RECIPE_CREATOR required');

    await expect(resourcesHandler.connect(recipeCreator).removePlotRURecipe(1))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 2, false);

    expect(await resourcesHandler.recipeExists(1,2)).to.be.equal(false);
  });
});



 describe("Test replenish/upgrade", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator, gameEconomy] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);

    var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
    var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
    var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
    var RECIPE_CREATOR = await resourcesHandler.RECIPE_CREATOR();
    var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();

    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);

    await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
    await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);

    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    await items.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
    
    await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
    await resourcesHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);

    await resourcesHandler.connect(owner).grantRole(RECIPE_CREATOR, recipeCreator.address);
    await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);

    await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);

    await planetPlotHandler.connect(gameControl).updateServer(server.address);
    await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);

    await planetPlotHandler.connect(creator).createPlanet(10, 100);
    await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
    await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');
    await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);
  });

  it("Test replenish", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1000000, [1], [1000], []);

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1], [1], 1000000, 0, 3))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 2, true);

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(2, [], [], 1000000, 0, 1000))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(2, 2, true);

    await expect(resourcesHandler.connect(player).plotRU(10, 100001001001))
    .to.be.revertedWith('Recipe id not exist');

    await expect(resourcesHandler.connect(player).plotRU(1, 100001001001))
    .to.be.revertedWith('ERC1155: caller is not owner nor approved');

    await expect(resourcesHandler.connect(player).plotRU(2, 100005001001))
    .to.be.revertedWith('ERC20: transfer amount exceeds allowance');

    //APPROVE ALLES
    await testDAR.connect(player).approve(resourcesHandler.address, 1000000000);
    await resources.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
    await items.connect(player).setApprovalForAll(resourcesHandler.address, player.address);

    //get some stuff for testing
    await expect(resourcesHandler.connect(player).craft(1, []))
    .to.emit(resourcesHandler, 'CraftEvent').withArgs(player.address, 1);

    // dig so we can test replenish
    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);
    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-11000000);
    expect((await planetPlot.plotState(100001001001)).left).to.be.equal(90);

    await expect(resourcesHandler.connect(player).plotRU(1, 100003001001))
    .to.be.revertedWith('Query for nonexistent plot');
    
    //notice that player can upgrade/replenish someone elses plot
    await expect(resourcesHandler.connect(player).plotRU(1, 100001001001))
    .to.emit(resourcesHandler, 'ReplenishUpgradeEvent').withArgs(1, 100001001001);
    
    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-12000000);
    expect((await planetPlot.plotState(100001001001)).left).to.be.equal(93);

    await expect(resourcesHandler.connect(player).plotRU(2, 100001001001))
    .to.emit(resourcesHandler, 'ReplenishUpgradeEvent').withArgs(2, 100001001001);

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-13000000);
    expect((await planetPlot.plotState(100001001001)).left).to.be.equal(100);

  });

  it("Test upgrade", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1000000, [1], [1000], []);

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1], [1], 1000000, 100, 50))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 2, true);

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(2, [], [], 1000000, 150, 25))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(2, 2, true);

    //APPROVE ALLES
    await testDAR.connect(player).approve(resourcesHandler.address, 1000000000);
    await resources.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
    await items.connect(player).setApprovalForAll(resourcesHandler.address, player.address);

    //get some stuff for testing
    await expect(resourcesHandler.connect(player).craft(1, []))
    .to.emit(resourcesHandler, 'CraftEvent').withArgs(player.address, 1);

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-1000000);

    await expect(resourcesHandler.connect(player).plotRU(2, 100001001001))
    .to.be.revertedWith('Plot max, recipe max mismatch');
    
    //notice that player can upgrade/replenish someone elses plot
    await expect(resourcesHandler.connect(player).plotRU(1, 100001001001))
    .to.emit(resourcesHandler, 'ReplenishUpgradeEvent').withArgs(1, 100001001001);
    
    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-2000000);
    expect((await planetPlot.plotState(100001001001)).max).to.be.equal(150);
    expect((await planetPlot.plotState(100001001001)).left).to.be.equal(150);

    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);
    await planetPlotHandler.connect(player).openRentPlot(100001001001, 5);

    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-12000000);
    expect((await planetPlot.plotState(100001001001)).max).to.be.equal(150);
    expect((await planetPlot.plotState(100001001001)).left).to.be.equal(140);

    await expect(resourcesHandler.connect(player).plotRU(1, 100001001001))
    .to.be.revertedWith('Plot max, recipe max mismatch');

    await expect(resourcesHandler.connect(player).plotRU(2, 100001001001))
    .to.emit(resourcesHandler, 'ReplenishUpgradeEvent').withArgs(2, 100001001001);
    
    expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-13000000);
    expect((await planetPlot.plotState(100001001001)).max).to.be.equal(175);
    expect((await planetPlot.plotState(100001001001)).left).to.be.equal(175);
  });

  it("Test pause", async function () {
    const [owner, creator, plotOwner, player, server, taxAccount,
      resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();

    await resourcesHandler.connect(recipeCreator).addCraftingRecipe(1, [], [], [], 1000000, [1], [1000], []);

    await expect(resourcesHandler.connect(recipeCreator).addPlotRURecipe(1, [1], [1], 1000000, 100, 50))
    .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 2, true);
    await testDAR.connect(player).approve(resourcesHandler.address, 1000000000);
    await resources.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
    await items.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
    await expect(resourcesHandler.connect(player).craft(1, []))
    .to.emit(resourcesHandler, 'CraftEvent').withArgs(player.address, 1);

    await planetPlotHandler.connect(pauser).pauseHandler();

    await expect(resourcesHandler.connect(player).plotRU(1, 100001001001))
    .to.be.revertedWith('Pausable: paused');

    await planetPlotHandler.connect(pauser).unpauseHandler();

    await resourcesHandler.connect(pauser).pauseHandler();

    await expect(resourcesHandler.connect(player).plotRU(1, 100001001001))
    .to.be.revertedWith('Pausable: paused');
  });
});


describe("Test planet pass", function () {
    var CNR;
    var testDAR;
    var planetPlot;
    var planetPlotHandler;
    var resources;
    var items;
    var resourcesHandler;
  
    beforeEach(async function () {
      CNR = await help.setCNR();
      testDAR = await help.setTestDAR();
      planetPlot = await help.setPlanetPlot(CNR);
      resources = await help.setResources();
      planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
      items = await help.setItems(CNR);
      const [owner, creator, plotOwner, player, server, taxAccount,
        resourceItemCreator, gameControl, pauser, recipeCreator, gameEconomy] = await ethers.getSigners();
      resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);
  
      var PLANET_PLOT_CREATOR = await planetPlotHandler.PLANET_PLOT_CREATOR();
      var PLANET_PLOT_HANDLER = await planetPlot.PLANET_PLOT_HANDLER();
      var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
      var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
      var GAME_CONTROL = await resourcesHandler.GAME_CONTROL();
      var GAME_PAUSER = await planetPlotHandler.GAME_PAUSER();
      var RECIPE_CREATOR = await resourcesHandler.RECIPE_CREATOR();
      var GAME_CONTROL = await planetPlotHandler.GAME_CONTROL();

      await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
      await resources.connect(owner).grantRole(RESOURCES_HANDLER, planetPlotHandler.address);
  
      await planetPlotHandler.connect(owner).grantRole(PLANET_PLOT_CREATOR, creator.address);
      await planetPlot.connect(owner).grantRole(PLANET_PLOT_HANDLER, planetPlotHandler.address);
      await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);
  
      await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
      await planetPlotHandler.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
      await items.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);
      
      await resourcesHandler.connect(owner).grantRole(GAME_CONTROL, gameControl.address);
      await planetPlotHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
      await resourcesHandler.connect(owner).grantRole(GAME_PAUSER, pauser.address);
  
      await resourcesHandler.connect(owner).grantRole(RECIPE_CREATOR, recipeCreator.address);
      await planetPlotHandler.connect(owner).grantRole(GAME_CONTROL, gameEconomy.address);
  
      await testDAR.connect(player).approve(planetPlotHandler.address, 1000000000);
  
      await planetPlotHandler.connect(gameControl).updateServer(server.address);
      await resourcesHandler.connect(gameControl).updatePlanetPlotHandler(planetPlotHandler.address);
  
      await planetPlotHandler.connect(creator).createPlanet(10, 100);
      await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 100, 1);
      await planetPlotHandler.connect(creator).createPlanet(10, 200);
      await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 200, 1);
      await planetPlotHandler.connect(creator).createPlanet(10, 300);
      await planetPlotHandler.connect(creator).mintPlotRegion(plotOwner.address, 300, 1);
      await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
      await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
      await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');
      await planetPlotHandler.connect(gameEconomy).setFreePlanetPass(100, true);
    });

    it("Test planet pass recipe", async function () {
      const [owner, creator, plotOwner, player, server, taxAccount,
        resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();
  
      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 0, [100]))
      .to.be.revertedWith('DAR input cant be 0');

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, []))
      .to.be.revertedWith('Must have 1+ planet output');

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, [400]))
      .to.be.revertedWith('Planet not exist');

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, [100, 200, 300, 400]))
      .to.be.revertedWith('Planet not exist');

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, [100, 200, 300]))
      .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 3, true);

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, [400]))
      .to.be.revertedWith('recipe id taken');

      await expect(resourcesHandler.connect(recipeCreator).removePlanetPassRecipe(1))
      .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 3, false);

      expect(await resourcesHandler.planetPassRecipes(1)).to.equal(0);

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, [100, 200, 300]))
      .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 3, true);
    });
  
    it("Test planet pass on rent", async function () {
      const [owner, creator, plotOwner, player, server, taxAccount,
        resourceItemCreator, gameControl, pauser, recipeCreator] = await ethers.getSigners();
  
      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(1, 1000000, [100]))
      .to.emit(resourcesHandler, 'RecipeEvent').withArgs(1, 3, true);

      await expect(resourcesHandler.connect(recipeCreator).addPlanetPassRecipe(2, 1000000, [200, 300]))
      .to.emit(resourcesHandler, 'RecipeEvent').withArgs(2, 3, true);
  
      await testDAR.connect(player).approve(resourcesHandler.address, 1000000000);
      await resources.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
      await items.connect(player).setApprovalForAll(resourcesHandler.address, player.address);
  
      await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent');
      await expect(planetPlotHandler.connect(player).openRentPlot(200001001001, 1)).to.be.revertedWith('Renter does not have a valid pass for this planet');
      await expect(planetPlotHandler.connect(player).openRentPlot(300001001001, 1)).to.be.revertedWith('Renter does not have a valid pass for this planet');
      
      await expect(resourcesHandler.connect(player).planetPass(1)).to.emit(resourcesHandler, 'PlanetPassEvent').withArgs(player.address, [100]);

      await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent');
      await expect(planetPlotHandler.connect(player).openRentPlot(200001001001, 1)).to.be.revertedWith('Renter does not have a valid pass for this planet');
      await expect(planetPlotHandler.connect(player).openRentPlot(300001001001, 1)).to.be.revertedWith('Renter does not have a valid pass for this planet');

      await expect(resourcesHandler.connect(player).planetPass(2)).to.emit(resourcesHandler, 'PlanetPassEvent').withArgs(player.address, [200,300]);

      await expect(planetPlotHandler.connect(player).openRentPlot(100001001001, 1)).to.emit(planetPlotHandler, 'Rent');
      await expect(planetPlotHandler.connect(player).openRentPlot(200001001001, 1)).to.emit(planetPlotHandler, 'Rent');
      await expect(planetPlotHandler.connect(player).openRentPlot(300001001001, 1)).to.emit(planetPlotHandler, 'Rent');

      expect(await planetPlotHandler.addressIsRenting(player.address)).to.be.equal(5);
      expect(await testDAR.balanceOf(player.address)).to.be.equal(100000000-7000000);
    });
  });