const { ethers } = require("hardhat");

async function setCNR(){
  const ChromiaNetResolver = await ethers.getContractFactory("ChromiaNetResolver");
  const chromiaNetResolver = await ChromiaNetResolver.deploy();
  await chromiaNetResolver.deployed();
  return chromiaNetResolver;
}

async function setItems(_cnr){
  const Items = await ethers.getContractFactory("Items");
  const items = await Items.deploy(_cnr.address);
  await items.deployed();
  return items;
}

async function setTestDAR(){
  const [owner, addr1, addr2, addr3, backEndServer, taxAccount] = await ethers.getSigners();
  const TestDAR = await ethers.getContractFactory("TestDAR");
  const testDAR = await TestDAR.deploy();
  await testDAR.deployed();
  await testDAR.connect(owner).faucet();
  // await testDAR.connect(addr1).faucet();
  await testDAR.connect(addr2).faucet();
  await testDAR.connect(addr3).faucet();
  await testDAR.connect(backEndServer).faucet();
  await testDAR.connect(taxAccount).faucet();
  return testDAR;
}

async function setPlanetPlot(_cnr){
  const PlanetPlot = await ethers.getContractFactory("PlanetPlot");
  const planetPlot = await PlanetPlot.deploy(_cnr.address);
  await planetPlot.deployed();
  return planetPlot;
}

async function setPlanetPlotHandler(_dar, _ppa, _r){
  const [owner, addr1, addr2, addr3, backEndServer, taxAccount] = await ethers.getSigners();
  const PlanetPlotHandler = await ethers.getContractFactory("PlanetPlotHandler");
  const planetPlotHandler = await PlanetPlotHandler.deploy(_r.address, _dar.address, _ppa.address, taxAccount.address, 10);
  await planetPlotHandler.deployed();
  return planetPlotHandler;
}

async function setResources(){
  const Resources = await ethers.getContractFactory("Resources");
  const resources = await Resources.deploy();
  await resources.deployed();
  return resources;
}

async function setResourcesHandler(_src, _items, _dar, _taxAccount, _planetPlot){
  const ResourcesHandler = await ethers.getContractFactory("ResourcesHandler");
  const resourcesHandler = await ResourcesHandler.deploy(_src.address, _items.address, _dar.address, _taxAccount.address, _planetPlot.address);
  await resourcesHandler.deployed();
  return resourcesHandler;
}

async function setSingleApproveTransfer(_dar, _r){
  const SingleApproveTransfer = await ethers.getContractFactory("SingleApproveTransfer");
  const singleApproveTransfer = await SingleApproveTransfer.deploy(_dar.address, _r.address);
  await singleApproveTransfer.deployed();
  return singleApproveTransfer;
}

async function setFactory(_dar, _r, _t){
  const Factory = await ethers.getContractFactory("Factory");
  const factory = await Factory.deploy(_dar.address, _r.address, _t.address);
  await factory.deployed();
  return factory;
}

module.exports = { setResourcesHandler, setResources, setPlanetPlotHandler, setPlanetPlot
  ,setTestDAR, setItems, setCNR, setFactory, setSingleApproveTransfer};


  // Oxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxO xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx

// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx

// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxOxxxxx xxxxxxxxxx 
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxO xxxxxxxxxx xxxxxxxxxx

// Oxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxx
// Oxxxxxxxxx xxxxxxxxxx xxxxxxxxxx xxxxxxxxxO