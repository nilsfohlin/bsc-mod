const { expect } = require("chai");
const { ethers } = require("hardhat");
const help = require("./test_helper_functions.js");

const ammAbi = [
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "previousAdminRole",
          "type": "bytes32"
        },
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "newAdminRole",
          "type": "bytes32"
        }
      ],
      "name": "RoleAdminChanged",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "account",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "sender",
          "type": "address"
        }
      ],
      "name": "RoleGranted",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "account",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "sender",
          "type": "address"
        }
      ],
      "name": "RoleRevoked",
      "type": "event"
    },
    {
      "inputs": [],
      "name": "DEFAULT_ADMIN_ROLE",
      "outputs": [
        {
          "internalType": "bytes32",
          "name": "",
          "type": "bytes32"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "FACTORY",
      "outputs": [
        {
          "internalType": "bytes32",
          "name": "",
          "type": "bytes32"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "LIQUIDITY_PROVIDER",
      "outputs": [
        {
          "internalType": "bytes32",
          "name": "",
          "type": "bytes32"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_darAmount",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_resourceAmount",
          "type": "uint256"
        }
      ],
      "name": "addLiquidity",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "dar",
      "outputs": [
        {
          "internalType": "contract IERC20",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "darReserve",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        }
      ],
      "name": "getRoleAdmin",
      "outputs": [
        {
          "internalType": "bytes32",
          "name": "",
          "type": "bytes32"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "grantRole",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "hasRole",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "contract IERC20",
          "name": "_dar",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "_resources",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "_resourceId",
          "type": "uint256"
        },
        {
          "internalType": "address",
          "name": "_ISingleApproveTransfer",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "_owner",
          "type": "address"
        }
      ],
      "name": "init",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "k",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "kMultiplier",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        },
        {
          "internalType": "uint256[]",
          "name": "",
          "type": "uint256[]"
        },
        {
          "internalType": "uint256[]",
          "name": "",
          "type": "uint256[]"
        },
        {
          "internalType": "bytes",
          "name": "",
          "type": "bytes"
        }
      ],
      "name": "onERC1155BatchReceived",
      "outputs": [
        {
          "internalType": "bytes4",
          "name": "",
          "type": "bytes4"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "bytes",
          "name": "",
          "type": "bytes"
        }
      ],
      "name": "onERC1155Received",
      "outputs": [
        {
          "internalType": "bytes4",
          "name": "",
          "type": "bytes4"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "price",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_percent",
          "type": "uint256"
        }
      ],
      "name": "removeLiquidity",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "renounceRole",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "resourceId",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "resourceReserve",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "resources",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "role",
          "type": "bytes32"
        },
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "revokeRole",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes4",
          "name": "interfaceId",
          "type": "bytes4"
        }
      ],
      "name": "supportsInterface",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_resourceIn",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_resourceOut",
          "type": "uint256"
        }
      ],
      "name": "swap",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "SingleApproveTransfer",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    }
  ];

describe("gold", function () {
  var CNR;
  var testDAR;
  var planetPlot;
  var planetPlotHandler;
  var resources;
  var items;
  var resourcesHandler;
  var factory;
  var SingleApproveTransfer;

  beforeEach(async function () {
    CNR = await help.setCNR();
    testDAR = await help.setTestDAR();
    planetPlot = await help.setPlanetPlot(CNR);
    resources = await help.setResources();
    planetPlotHandler = await help.setPlanetPlotHandler(testDAR, planetPlot, resources);
    items = await help.setItems(CNR);
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator] = await ethers.getSigners();
    resourcesHandler = await help.setResourcesHandler(resources, items, testDAR, taxAccount, planetPlot);
    SingleApproveTransfer = await help.setSingleApproveTransfer(testDAR, resources);
    factory = await help.setFactory(testDAR, resources, SingleApproveTransfer);

    var RESOURCE_ITEM_CREATOR = await resourcesHandler.RESOURCE_ITEM_CREATOR();
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();

    await resourcesHandler.connect(owner).grantRole(RESOURCE_ITEM_CREATOR, resourceItemCreator.address);
    await resources.connect(owner).grantRole(RESOURCES_HANDLER, resourcesHandler.address);

    await resourcesHandler.connect(resourceItemCreator).createResource('GOLD');
    await resourcesHandler.connect(resourceItemCreator).createResource('SILVER');
    await resourcesHandler.connect(resourceItemCreator).createResource('COPPER');

  });

  it("first test", async function () {
    var RESOURCES_HANDLER = await resources.RESOURCES_HANDLER();
    
    const [owner, creator, plotOwner, player, server, taxAccount, gameEconomy, resourceItemCreator, gameControl, pauser] = await ethers.getSigners();

    var FACTORY = await SingleApproveTransfer.FACTORY();
    await SingleApproveTransfer.connect(owner).grantRole(FACTORY, factory.address);

    var FACTORY_HANDLER = await factory.FACTORY_HANDLER();
    await factory.connect(owner).grantRole(FACTORY_HANDLER, owner.address);

    await factory.connect(owner).createPair(0);

    let goldA = await factory.getPair(0);

    const gold = new ethers.Contract(goldA, ammAbi, owner);

    await testDAR.connect(owner).faucet10000();

    await resources.connect(owner).grantRole(RESOURCES_HANDLER, gold.address);

    await testDAR.connect(owner).approve(gold.address, 100000000000);

    var LIQUIDITY_PROVIDER = await gold.LIQUIDITY_PROVIDER();
    await gold.connect(owner).grantRole(LIQUIDITY_PROVIDER, owner.address);

    await gold.connect(owner).addLiquidity(10000000000, 100000);

    console.log((await gold.k()).toNumber())

    await testDAR.connect(player).approve(SingleApproveTransfer.address, 100000000000);

    // console.log(gold.address);

    await gold.connect(player).swap(0, 100);

    console.log((await testDAR.balanceOf(player.address)).toNumber())
    console.log((await resources.balanceOf(player.address, 0 )).toNumber())
    console.log((await gold.k()).toNumber())






    await factory.connect(owner).createPair(1);

    let silverA = await factory.getPair(1);

    const silver = new ethers.Contract(silverA, ammAbi, owner);

    await testDAR.connect(owner).faucet10000();

    await resources.connect(owner).grantRole(RESOURCES_HANDLER, silver.address);

    await testDAR.connect(owner).approve(silver.address, 100000000000);

    await silver.connect(owner).grantRole(LIQUIDITY_PROVIDER, owner.address);

    await silver.connect(owner).addLiquidity(10000000000, 100000);

    await testDAR.connect(player).approve(silver.address, 100000000000);

    await silver.connect(player).swap(0, 100);

    console.log((await testDAR.balanceOf(player.address)).toNumber())
    console.log((await resources.balanceOf(player.address, 1 )).toNumber())






    await resources.connect(player).setApprovalForAll(SingleApproveTransfer.address, true);

    await silver.connect(player).swap(25, 0);

    console.log((await testDAR.balanceOf(player.address)).toNumber())
    console.log((await resources.balanceOf(player.address, 1 )).toNumber())

    await gold.connect(player).swap(25, 0);

    console.log((await testDAR.balanceOf(player.address)).toNumber())
    console.log((await resources.balanceOf(player.address, 0 )).toNumber())

    console.log("*********************LINE*********************")
    console.log((await testDAR.balanceOf(silver.address)).toNumber())
    console.log((await resources.balanceOf(silver.address, 1 )).toNumber())
    console.log((await silver.k()).toNumber())
    console.log("********************remove********************")

    await silver.removeLiquidity(50);
    console.log((await testDAR.balanceOf(silver.address)).toNumber())
    console.log((await resources.balanceOf(silver.address, 1 )).toNumber())
    console.log((await silver.k()).toNumber())
    
  });
});