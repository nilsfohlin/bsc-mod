async function setCNR(){
  const ChromiaNetResolver = await ethers.getContractFactory("ChromiaNetResolver");
  const chromiaNetResolver = await ChromiaNetResolver.deploy();
  await chromiaNetResolver.deployed();
  return chromiaNetResolver;
}

async function setItems(_cnr){
  const Items = await ethers.getContractFactory("Items");
  const items = await Items.deploy(_cnr.address);
  await items.deployed();
  return items;
}

async function setTestDAR(){
  const TestDAR = await ethers.getContractFactory("TestDAR");
  const testDAR = await TestDAR.deploy();
  await testDAR.deployed();
  return testDAR;
}

async function setPlanetPlot(_cnr){
  const PlanetPlot = await ethers.getContractFactory("PlanetPlot");
  const planetPlot = await PlanetPlot.deploy(_cnr.address);
  await planetPlot.deployed();
  return planetPlot;
}

async function setPlanetPlotHandler(_dar, _ppa, _r){
  const PlanetPlotHandler = await ethers.getContractFactory("PlanetPlotHandler");
  const planetPlotHandler = await PlanetPlotHandler.deploy(_r.address, _dar.address, _ppa.address, '0x8b0cbF0ED2B76E0a284e58b58F74589e9513641D', 10);
  await planetPlotHandler.deployed();
  return planetPlotHandler;
}

async function setResources(){
  const Resources = await ethers.getContractFactory("Resources");
  const resources = await Resources.deploy();
  await resources.deployed();
  return resources;
}

async function setResourcesHandler(_src, _items, _dar, _planetPlot){
  const ResourcesHandler = await ethers.getContractFactory("ResourcesHandler");
  const resourcesHandler = await ResourcesHandler.deploy(_src.address, _items.address, _dar.address, '0x8b0cbF0ED2B76E0a284e58b58F74589e9513641D', _planetPlot.address);
  await resourcesHandler.deployed();
  return resourcesHandler;
}


async function main() {
  const CNR = await setCNR();
  const testDAR = await setTestDAR();
  const items = await setItems(CNR);
  const planetPlot = await setPlanetPlot(CNR);
  const resources = await setResources();
  const planetPlotHandler = await setPlanetPlotHandler(testDAR, planetPlot, resources);
  const resourcesHandler = await setResourcesHandler(resources, items, testDAR, planetPlot);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });


  // npx hardhat run scripts/deploy-testnet.js --network BSCTestnet
  // npx hardhat verify --network BSCTestnet contractAddress paramaters 